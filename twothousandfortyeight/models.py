# -*- coding: utf8 -*-
"""
twothousandfortyeight.models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This module contains definitions of the types in the game.
"""

from copy import deepcopy
from enum import Enum
from random import random, choice


class Moves(Enum):
    """Represents the moves that a player can perform."""
    up = 1
    down = 2
    right = 3
    left = 4


class Tile(object):
    """Represents a tile on the board.

    All tiles are initialized without value"""

    def __init__(self, value=None):
        self.value = value
        self.converted = False

    def __eq__(self, other):
        return self.value == other.value


class Board(object):
    """Represents the board, holding the tiles and score of the board."""

    def __init__(self, width=4, height=4, initial_values=None, score=0):
        self.tiles = []
        self.score = score
        self.width = width
        self.height = height
        for x in range(0, self.width):
            self.tiles.append([])
            for y in range(0, self.height):
                tile_value = initial_values.get((x, y)) if initial_values \
                    else None

                self.tiles[x].append(Tile(tile_value))

        for x in range(0, self.width):
            for y in range(0, self.height):
                if x > 0:
                    self.tiles[x][y].left = self.tiles[x - 1][y]
                if x < self.width - 1:
                    self.tiles[x][y].right = self.tiles[x + 1][y]
                if y > 0:
                    self.tiles[x][y].down = self.tiles[x][y - 1]
                if y < self.height - 1:
                    self.tiles[x][y].up = self.tiles[x][y + 1]
        if not initial_values:
            self.add_new_value()
            self.add_new_value()

    def __eq__(self, other):
        for i in range(len(self.tiles)):
            for j in range(len(self.tiles[i])):
                if not self.tiles[i][j] == other.tiles[i][j]:
                    return False
        return True

    def __str__(self):
        dashes = 7 * self.width + 1  # 7 dashes per tile and one
        # extra for the border
        result = '-' * dashes + '\n'
        for y in reversed(range(self.height)):
            result += '| '
            for x in range(self.width):
                tile = self.tiles[x][y]
                result += str(tile.value).ljust(4) if tile.value \
                    else ''.ljust(4)
                result += " | "
            result += '\n' + '-' * dashes + '\n'
        result += 'Score: ' + str(self.score) + '\n'
        return result

    def __reset_converted(self):
        for row in self.tiles:
            for tile in row:
                tile.converted = False

    def __horizontal(self, x_range, y_range, direction):
        for x in x_range:
            for y in y_range:
                self.__move_items(self.tiles[x][y], direction)
        self.__reset_converted()

    def __move_items(self, tile, direction):
        while hasattr(tile, direction.name):
            next_tile = getattr(tile, direction.name)
            if next_tile.value:
                if not tile.converted and next_tile.value == tile.value:
                    next_tile.value = tile.value * 2
                    self.score += tile.value * 2
                    next_tile.converted = True
                else:
                    break
            else:
                getattr(tile, direction.name).value = tile.value
            tile.value = None
            tile = getattr(tile, direction.name)

    def __vertical(self, x_range, y_range, direction):
        for y in y_range:
            for x in x_range:
                self.__move_items(self.tiles[x][y], direction)
        self.__reset_converted()

    @property
    def free_tiles(self):
        """Return all the free tiles (i.e. tiles without a designated value)"""
        return [tile for row in self.tiles for tile in row if not tile.value]

    @property
    def valid_moves(self):
        result = []
        for direction in Moves:
                test_board = deepcopy(self)
                getattr(test_board, 'shift_%s' % direction.name)()

                if not self == test_board:
                    result.append(direction)

        return result

    def add_new_value(self):
        """Add a new value to a random tile on the board. The new value will
        in 90% of the cases be 2 and 4 in the remaining cases."""
        free_tiles = self.free_tiles
        new_value = 2
        if random() > 0.9:
            new_value = 4
        choice(free_tiles).value = new_value

    def shift_down(self):
        """Shift all tiles on the board down"""
        self.__vertical(range(self.width), range(1, self.height),
                        Moves.down)

    def shift_left(self):
        """Shift all tiles on the board to the left"""
        self.__horizontal(range(1, self.width), range(self.height), Moves.left)

    def shift_right(self):
        """Shift all tiles on the board to the right"""
        self.__horizontal(range(self.width - 2, -1, -1), range(self.height),
                          Moves.right)

    def shift_up(self):
        """Shift all the tiles on the board up"""
        self.__vertical(range(self.width), range(self.height - 2, -1, -1),
                        Moves.up)

    @classmethod
    def from_an(cls, an_formatted_board, score=0):
        values = {}
        for coordinates, value in an_formatted_board.items():
            values[(ord(coordinates[0]) - 97, int(coordinates[1]) - 1)] = value

        return cls(initial_values=values, score=score)

    def to_an(self):
        converted_tiles = {}
        for x in range(self.width):
            for y in range(self.height):
                if self.tiles[x][y].value:
                    converted_tiles['%c%d' % (x + 97, y + 1)] = \
                        self.tiles[x][y].value

        return converted_tiles


class Player(object):
    def __init__(self, board, decider=None):
        self.board = board
        self.decider = decider

    @property
    def is_alive(self):
        return len(self.board.valid_moves) > 0

    def make_move(self):
        move = self.decider(self.board, self.board.valid_moves)
        getattr(self.board, "shift_%s" % move.name)()
        if len(self.board.free_tiles) > 0:
            self.board.add_new_value()

    def __str__(self):
        return str(self.board)
