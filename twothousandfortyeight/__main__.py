#!/usr/bin/python3
# -*- coding: utf8 -*-
"""
twothousandfortyeight.__main__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the module is invoked directly, then we start the game loop.
"""


import sys
from importlib import import_module
from inspect import getargspec
from itertools import islice

from .game import play


def main():
    # If the module is invoked without other arguments, activate the default
    # ai, else fetch the specified decider from the argv

    players = []

    if len(sys.argv) > 1:
        command_iter = iter(sys.argv)
        next(command_iter)

        for argument in command_iter:
            parts = argument.split('.')
            obj = None

            for part in parts:
                if obj:
                    try:
                        obj = getattr(obj, part)

                    except:
                        obj = import_module('.'.join((obj.__name__, part)))
                else:
                    obj = import_module(part)

            decider = obj

            obj.args = list(islice(command_iter,
                                   len(getargspec(decider).args) - 2))

            players.append(obj)

    else:
        from .ai import get_next_move
        players = [get_next_move]

    play(players)

if __name__ == "__main__":
    main()
