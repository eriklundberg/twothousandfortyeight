from random import choice
from enum import Enum
import json
import requests

from .models import Player


class EnumEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Enum):
            return o.name
        return json.JSONEncoder.default(self, o)


class HTTPPlayer(Player):

    def __init__(self, board, url):
        super(HTTPPlayer, self).__init__(board)
        self.url = url

    def make_move(self):

        payload = json.dumps({
            'tiles': self.board.to_an(),
            'valid_moves': self.board.valid_moves,
            'score': self.board.score
        }, cls=EnumEncoder)

        from time import sleep
        sleep(0.25)

        r = requests.post(self.url, data=payload, headers={
            'Content-Type': 'application/json'
        })

        getattr(self.board, 'shift_%s' % r.text)()

        self.board.add_new_value()

    def __str__(self):
        return self.url.ljust(30) + "\n" + str(self.board)
