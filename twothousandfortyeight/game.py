# -*- coding: utf8 -*-
"""
twothousandthirtyeight.game
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This module contains the game logic of the 2048 puzzle.
"""

from .models import Board, Moves, Player
from random import randint
from inspect import isfunction


def play(ais):
    """
    This function implements the game loop by creating a new board and then
    call the function passed as parameter to get the next move. The loop will
    run as long as there is a valid move to make.

    :param get_next_move: The function that will determine with moves to make.
    """
    initial_tiles = {
        (randint(0, 3), randint(0, 3)): 2,
        (randint(0, 3), randint(0, 3)): 2
    }

    players = []
    for player in ais:
        if isfunction(player):
            players.append(Player(Board(initial_values=initial_tiles), player))

        else:
            players.append(player(Board(initial_values=initial_tiles),
                                  *player.args))

    while any([player.is_alive for player in players]):
        for player in players:
            if not player.is_alive:
                continue

            player.make_move()

        a = [str(player).split('\n') for player in players]
        for j in range(len(a[0])):
            for i in range(len(players)):
                print(a[i][j].ljust(30), end=' ')
            print()

    return {
        player: player.board.score for player in players
    }
