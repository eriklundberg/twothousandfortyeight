# -*- coding: utf8 -*-
"""
twothousandfortyeight.ai
~~~~~~~~~~~~~~~~~~~~~~~~

This module contains a sample implementation of a 2048 ai.
"""

from copy import deepcopy


def __evaluate_option(board):
    """Apply metric to determine how good a move option is"""
    return board.score


def get_next_move(board, valid_moves):
    """Take the current board, identify options, apply metrics and select
    the best"""

    options = {move: deepcopy(board) for move in valid_moves}

    for option in options:
        getattr(options[option], 'shift_%s' % option.name)()

    calculated_options = {__evaluate_option(options[d]): d for d in options
                          if not options[d].tiles == board.tiles}

    return calculated_options[max(calculated_options)]
