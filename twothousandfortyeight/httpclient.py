from flask import Flask, request
from random import choice
from .models import Board
from copy import deepcopy

app = Flask(__name__)


def evaluate(board):
    metric = 0
    multipliers = [
        [1, 1,   2,   10],
        [1, 1, 0.5,   2],
        [-1, 1, 1, 1],
        [-2, -1,   1,   1]
    ]

    for x in range(len(multipliers)):
        for y in range(len(multipliers)):
            if board.tiles[x][y].value:

                metric += board.tiles[x][y].value * multipliers[x][y] \
                    + len(board.free_tiles) * 17
    return metric


@app.route('/', methods=['POST'])
def move():
    context = request.get_json()
    board = Board.from_an(context['tiles'], score=context['score'])

    print(context['valid_moves'])

    directions = {name: deepcopy(board) for name in context['valid_moves']}

    for direction in directions:
        getattr(directions[direction], 'shift_%s' % direction)()

    evaluated_directions = {evaluate(d): name
                            for name, d in directions.items()}
    print(evaluated_directions)

    return evaluated_directions[max(evaluated_directions.keys())]


if __name__ == "__main__":
    app.debug = False
    app.run()
