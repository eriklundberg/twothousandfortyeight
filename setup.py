from setuptools import setup

setup(
    name='twothousandfortyeight',
    version='1',
    packages=['twothousandfortyeight'],
    install_requires=['flask', 'requests'],
    url='',
    license='',
    author='Erik Lundberg',
    author_email='lundbergerik@gmail.com',
    description='',
    entry_points={
        'console_scripts': [
            'twothousandfortyeight = twothousandfortyeight.__main__:main'
        ]
    }
)
